
var MAIN = (function () {

    var list = document.querySelector("#listProducts");

    var listProducts = function () {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', 'http://localhost:8000/viaVarejo', true);

        xhr.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var response = this.responseText;
                var parse = JSON.parse(response);
                listProductsSucess(parse);
            } 
        }
        xhr.send();
        xhr.status;
    }

    var listProductsSucess = function(products) {
        var itens = []

        products.forEach(function(product) {
            product.menus.forEach(function(menus) {
                menus.itens.forEach(function(itensList) {
                    itens.push('<li>' + itensList.name + ' (' + itensList.count + ') ' + '</li>');
                })
            });
        });

        var newArr = itens.sort();
        list.innerHTML = newArr.join('');
    }

    var init = function() {
        listProducts();
    }();
})();

$(document).ready(function () {
    $('.owl-carousel').owlCarousel({
        items: 3,
        loop: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            300: {
                items: 1,
                nav: false
            },
            850: {
                items: 3,
                nav: true,
                loop: false
            }
        }
    })
});

